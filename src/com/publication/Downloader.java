package com.publication;

import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

class Downloader extends Thread {
    private File destination;
    private DownloaderGUI downloaderGUI;
    private ProgressBarAndDownloadButtonUpdater progressBarAndDownloadButtonUpdater = null;

    Downloader(DownloaderGUI downloaderGUI) {
        this.downloaderGUI = downloaderGUI;
    }

    private void download(String downloadLink, String downloadPath, String fileName) {
        String filePath = downloadPath;
        String pubType = fileName.substring(fileName.lastIndexOf('_') + 1, fileName.lastIndexOf('.'));
        switch (pubType) {
            case "w":
                filePath += "watchtower_study/";
                break;
            case "g":
                filePath += "awake/";
                break;
            case "mwb":
                filePath += "workbook/";
                break;
            case "wp":
                filePath += "watchtower_public/";
                break;
            default:
                filePath += "books/";
                break;
        }

        filePath += fileName;
        destination = new File(filePath);
        System.out.println("Download Started to file: " + filePath);

        try {
            URL url = new URL(downloadLink);
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());

            float fileSize = httpConnection.getContentLength();
            int decision;
            if (fileSize != -1) {
                decision = JOptionPane.showConfirmDialog(downloaderGUI.getFrame(), "Download " + String.format("%.2f", fileSize / 1048576) + " MB(s)?", "File size", JOptionPane.YES_NO_OPTION);
                switch (decision) {
                    case JOptionPane.OK_OPTION:
                        break;
                    case JOptionPane.NO_OPTION:
                        return;
                }
            } else {
                JOptionPane.showMessageDialog(downloaderGUI.getFrame(), "Publication may not be available", "Unknown error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            System.out.println("File size:\t" + fileSize / 1048576 + " MB(s)");
            if (fileSize > 0) {
                this.downloaderGUI.getCancelButton().setEnabled(true);
                this.downloaderGUI.getDownloadButton().setEnabled(false);
            }

            progressBarAndDownloadButtonUpdater = new ProgressBarAndDownloadButtonUpdater(fileSize, filePath, downloaderGUI);
            progressBarAndDownloadButtonUpdater.start();
            FileUtils.copyURLToFile(url, destination, 120000 /* connection timeout */, 300000 /* read timeout */);
        } catch (IOException e) {
            if (progressBarAndDownloadButtonUpdater != null) {
                progressBarAndDownloadButtonUpdater.setRunningState(false);
                this.downloaderGUI.getCancelButton().setEnabled(false);
                this.downloaderGUI.getDownloadButton().setEnabled(true);
            }
            JOptionPane.showMessageDialog(downloaderGUI.getFrame(), "Connection timed out", "Connection Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void run () {
        /* Change 'downloadPath' to a field variable
         if you're adding a 'change download directory' feature */
        String downloadPath = "/home/miki/Desktop/Downloaded Publication/";
        download(downloaderGUI.getUrlString(), downloadPath, downloaderGUI.getFileName());
    }

    void stopDownload() {
        this.stop();
        this.downloaderGUI.getCancelButton().setEnabled(false);
        this.downloaderGUI.getDownloadButton().setEnabled(true);
        progressBarAndDownloadButtonUpdater.setRunningState(false);

        if ((int) ((progressBarAndDownloadButtonUpdater.getDownloaded() / progressBarAndDownloadButtonUpdater.getTotalSize()) * 100) < 100)
            progressBarAndDownloadButtonUpdater.getDownloaderGUI().updateProgressBar(0);
        try {
            FileUtils.forceDelete(destination);
        } catch (IOException ignored) { }
    }
}

// ************************************************************************ //

class ProgressBarAndDownloadButtonUpdater extends Thread {
    private float totalSize;
    private float downloaded = 0;
    private String filePath;
    private DownloaderGUI downloaderGUI;
    private boolean runningState = true;

    ProgressBarAndDownloadButtonUpdater(float totalSize, String filePath, DownloaderGUI downloaderGUI) {
        this.totalSize = totalSize;
        this.filePath = filePath;
        this.downloaderGUI = downloaderGUI;
    }

    @Override
    public void run() {
        File file = new File(filePath);
        while (downloaded < totalSize && runningState)
            try {
                downloaded = file.length();
                downloaderGUI.updateProgressBar((int) (downloaded / totalSize * 100));
                System.out.print((int) ((downloaded / totalSize) * 100) + "% downloaded...");
                Thread.sleep(500);
                System.out.print("\r");
            } catch (InterruptedException ignored) { }

        if (downloaded == totalSize) {
            downloaderGUI.getDownloadButton().setEnabled(true);
            downloaderGUI.getCancelButton().setEnabled(false);
            JOptionPane.showMessageDialog(downloaderGUI.getFrame(), "Download Complete!");
        }
        totalSize = 0;
    }

    public void setRunningState(boolean runningState) {
        this.runningState = runningState;
    }

    public float getTotalSize() {
        return totalSize;
    }

    public float getDownloaded() {
        return downloaded;
    }

    public DownloaderGUI getDownloaderGUI() {
        return downloaderGUI;
    }
}
