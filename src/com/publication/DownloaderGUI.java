package com.publication;

import org.json.JSONObject;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;

public class DownloaderGUI {

    private final JFrame frame;
    private final JButton cancelButton;
    private final JButton downloadButton;
    private final JProgressBar progressBar;
    private String urlString;
    private String pub = "";
    private String fileName = "";
    private Downloader downloader;

    private DownloaderGUI() {
        frame = new JFrame("Publication Downloader");
        //frame.setLayout(new GridLayout(4, 1));

        JTabbedPane tabbedPane = new JTabbedPane();
        JPanel magazinePanel = new JPanel(new GridLayout(4, 1));
        JPanel bookPanel = new JPanel(new GridLayout(4, 1));

        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem exit = new JMenuItem("Exit");
        JMenu help = new JMenu("Help");
        JMenuItem about = new JMenuItem("About...");

        @SuppressWarnings("Convert2Lambda") ActionListener exitOption = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        };
        exit.addActionListener(exitOption);

        @SuppressWarnings("Convert2Lambda") ActionListener aboutOption = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "This cool software was designed by Mike!");
            }
        };
        about.addActionListener(aboutOption);

        help.add(about);
        file.add(exit);
        menuBar.add(file);
        menuBar.add(help);
        frame.setJMenuBar(menuBar);

        progressBar = new JProgressBar(0, 100);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.gray);

        JLabel pubTypeLabel = new JLabel("<html>Pub<br/>Type:</html>");
        JLabel monthLabel = new JLabel("Month:");
        JLabel fileTypeLabel = new JLabel("<html>File<br/>Type:</html>");
        JLabel yearLabel = new JLabel("Year:");

        JPanel pubTypeMonthPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        pubTypeMonthPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        JPanel fileTypeYearPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        fileTypeYearPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        JPanel downloadCancelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        downloadButton = new JButton("Download");
        cancelButton = new JButton("Cancel");
        cancelButton.setEnabled(false);

        ImageIcon downloadImage = new ImageIcon("downloadImage.png");
        downloadButton.setIcon(downloadImage);

        ImageIcon cancelImage = new ImageIcon("cancelImage.png");
        cancelButton.setIcon(cancelImage);

        JPanel progressPanel = new JPanel((new GridLayout()));

        // Month/Year arrays
        Integer[] allMonths          = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        Integer[] allYears           = {2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010,
                                        2009, 2008, 2007, 2006, 2005, 2004, 2003, 2002, 2001, 2000};
        Integer[] w_month_2018       = {1, 5, 9};
        Integer[] g_month_2018       = {3, 7, 11};
        Integer[] w_month_2016_17    = {1, 3, 5, 7, 9, 11};
        Integer[] g_month_2016_17    = {2, 4, 6, 8, 10, 12};
        Integer[] christianLifeYears = {2019, 2018, 2017, 2016};
        // ******************

        String[] pubTypeItemsPost2016 = {"Watchtower - Study", "Watchtower - Public", "Christian Life", "Awake"};
        String[] pubTypeItemsPre2016 = {"Watchtower - Study", "Watchtower - Public", "Awake"};
        JComboBox<String> pubType = new JComboBox<>(pubTypeItemsPre2016);
        pubTypeLabel.setLabelFor(pubType);

        JComboBox<Integer> month = new JComboBox<>(allMonths);
        monthLabel.setLabelFor(month);

        String[] fileTypes = {"JWPUB", "PDF", "EPUB"};
        JComboBox<String> fileType = new JComboBox<>(fileTypes);
        fileTypeLabel.setLabelFor(fileType);

        JComboBox<Integer> year = new JComboBox<>(allYears);
        yearLabel.setLabelFor(year);

        pubTypeMonthPanel.add(pubTypeLabel);
        pubTypeMonthPanel.add(pubType);
        pubTypeMonthPanel.add(monthLabel);
        pubTypeMonthPanel.add(month);

        fileTypeYearPanel.add(fileTypeLabel);
        fileTypeYearPanel.add(fileType);
        fileTypeYearPanel.add(yearLabel);
        fileTypeYearPanel.add(year);

        downloadButton.addActionListener((ActionEvent e) -> {
            switch (Objects.requireNonNull(pubType.getSelectedItem()).toString()) {
                case "Watchtower - Study":
                    pub = "w";
                    break;
                case "Watchtower - Public":
                    pub = "wp";
                    break;
                case "Awake":
                    pub = "g";
                    break;
                case "Christian Life":
                    pub = "mwb";
                default:
                    break;
            }

            int selectedMonth = Integer.parseInt(Objects.requireNonNull(month.getSelectedItem()).toString());
            char zero = (selectedMonth >= 10) ? '\0' : '0';

            //noinspection SpellCheckingInspection,SpellCheckingInspection,SpellCheckingInspection
            urlString = "https://apps.jw.org/GETPUBMEDIALINKS?issue=" + Objects.requireNonNull(year.getSelectedItem()).toString()
                    + zero + month.getSelectedItem().toString() + "&output=json&pub=" + pub + "&fileformat=" +
                    Objects.requireNonNull(fileType.getSelectedItem()).toString() + "&alllangs=0&langwritten=AM&txtCMSLang=AM";
            fileName = year.getSelectedItem().toString() + "_" + zero + month.getSelectedItem().toString() + "_" + pub;

            try {
                String typeInJSONLink = "";
                URL oracle = new URL(urlString);
                BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
                StringBuilder jsonContent = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    jsonContent.append(inputLine);
                }

                in.close();

                switch (fileType.getSelectedItem().toString()) {
                    case "JWPUB":
                        typeInJSONLink = "JWPUB";
                        fileName += ".jwpub";
                        break;
                    case "PDF":
                        typeInJSONLink = "PDF";
                        fileName += ".pdf";
                        break;
                    case "EPUB":
                        typeInJSONLink = "EPUB";
                        fileName += ".epub";
                        break;
                    default:
                        break;
                }

                urlString = new JSONObject(jsonContent.toString())
                        .getJSONObject("files")
                        .getJSONObject("AM")
                        .getJSONArray(typeInJSONLink)
                        .getJSONObject(0)
                        .getJSONObject("file").get("url").toString();

                if (!urlString.isEmpty()) {
                    System.out.println(urlString);
                    downloader = new Downloader(this);
                    downloader.start();
                }

            } catch (Exception ignore) {}
        });

        cancelButton.addActionListener(e -> downloader.stopDownload());

        month.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                String publication = Objects.requireNonNull(pubType.getSelectedItem()).toString();
                int year_now = Integer.parseInt(Objects.requireNonNull(year.getSelectedItem()).toString());

                if ((publication.equals("Watchtower - Public") || publication.equals("Awake") )
                        && (year_now > 2015 && year_now < 2018))
                {
                    month.setModel(new DefaultComboBoxModel<>(w_month_2016_17));
                    monthLabel.setText("Number:");
                } else if (publication.equals("Awake") && (year_now > 2015 && year_now < 2018)) {
                    month.setModel(new DefaultComboBoxModel<>(g_month_2016_17));
                    monthLabel.setText("Number:");
                } else if (publication.equals("Watchtower - Public") && year_now > 2017) {
                    month.setModel(new DefaultComboBoxModel<>(w_month_2018));
                } else if (publication.equals("Awake") && year_now > 2017) {
                    month.setModel(new DefaultComboBoxModel<>(g_month_2018));
                } else
                    month.setModel(new DefaultComboBoxModel<>(allMonths));
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        year.addPopupMenuListener (new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                String publication = Objects.requireNonNull(pubType.getSelectedItem()).toString();

                if (publication.equals("Christian Life")) {
                    month.setModel(new DefaultComboBoxModel<>(allMonths));
                    year.setModel(new DefaultComboBoxModel<>(christianLifeYears));
                } else year.setModel(new DefaultComboBoxModel<>(allYears));
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        pubType.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                int year_now = Integer.parseInt(Objects.requireNonNull(year.getSelectedItem()).toString());
                if (year_now < 2016)
                    pubType.setModel(new DefaultComboBoxModel<>(pubTypeItemsPre2016));
                else
                    pubType.setModel(new DefaultComboBoxModel<>(pubTypeItemsPost2016));
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        downloadCancelPanel.add(cancelButton);
        downloadCancelPanel.add(downloadButton);

        progressPanel.add(progressBar);

        magazinePanel.add(pubTypeMonthPanel);
        magazinePanel.add(fileTypeYearPanel);
        magazinePanel.add(progressPanel);
        magazinePanel.add(downloadCancelPanel);

        tabbedPane.addTab("Mags & Workbooks", magazinePanel);
        tabbedPane.addTab("Books", bookPanel);

        frame.add(tabbedPane);

        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(3);
    }

    public static void main(String[] args) {
        @SuppressWarnings("unused")
        DownloaderGUI downloaderGUI = new DownloaderGUI();
    }

    public JFrame getFrame() {
        return frame;
    }

    public JButton getDownloadButton() {
        return downloadButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public void updateProgressBar(int progress) {
        progressBar.setValue(progress);
    }

    public String getUrlString() {
        return urlString;
    }

    public String getFileName() {
        return fileName;
    }
}